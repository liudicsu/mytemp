#coding=utf-8
from bert_serving.client import BertClient
import bert_serving.client
import numpy as np
import sys

#print(bert_serving.client.__file__)
def get_input(line_num=1):
	lines = []
	nodes = []	
	l_n = 1
	for line in sys.stdin:
		sps = line.strip().split("\t")	
		node = sps[0]
		data = sps[1]
		lines.append(data)
		nodes.append(node)
		if l_n % line_num == 0:
			yield lines, nodes
			lines.clear()
			nodes.clear()
		l_n += 1
	if len(lines) != 0:
		yield lines, nodes
        

def write_file(nodes, lines, array, wr):
	for node, line, vector in zip(nodes, lines, array):
		tmp = [node, line]
		tmp.extend(str(i) for i in vector)
		wr.write("\t".join(tmp))	
		wr.write("\n")
		

if __name__ == "__main__":
	bc = BertClient()
	line_num = 0

	with open(sys.argv[1]+".contact", "w") as wr_c:
		with open(sys.argv[1]+".mean", "w") as wr_m:
			for lines, nodes in get_input(128):
				line_num += 128
				if line_num % 1024 == 0:
					print("line is {}".format(line_num))
				try:
					array = bc.encode(lines)
					#print(array.shape)
					norm_np = np.linalg.norm(array, axis=1)
					#print(norm_np.shape)
					shape_p = norm_np.shape
					c_array = array/norm_np.reshape(shape_p[0],1)
					write_file(nodes, lines, c_array, wr_c)
					
					a_shape = int(array.shape[1] / 768)
					#print(a_shape)
					c_array = c_array[:, 0:768]
					c_array[:] = 0.0
					for i in range(a_shape):	
						c_array += array[:, i*768:(i+1)*768]		
					norm_np = np.linalg.norm(c_array, axis=1)
					c_array = c_array/norm_np.reshape(shape_p[0],1)
					write_file(nodes, lines, c_array, wr_m)
				except Exception as e:
					raise
					print(e)
