#coding=utf-8

import util
import os
from collections import Counter
import filter_file_load
import sys

parse_file_dict={}

def load_query_node(node_query_file, no_need_label_set):
    q2n_dict = {}
    with open(node_query_file) as nqf:
        for line in nqf:
            sps = line.strip().split("\t")
            query = sps[1]
            node = sps[0]
            query = util.clean_without_space(query).encode("utf-8")
            q2n_dict[query] = node
            no_need_label_set.add(query)
    return q2n_dict


def get_file_list(path_dir):
    parents = os.listdir(path_dir)
    result = [] 
    for file_name in parents:
        if file_name.endswith(".txt"):
            child = os.path.join(path_dir, file_name)
    result.append(child) 
    return result


def is_vaild_biaozhu(biaozhu_result, vailed_count=2):
    cnt  = Counter(biaozhu_result)
    for key in cnt:
        if key != "" and cnt[key] >= vailed_count:
            return key
    return None

def default_parse_handle(q2n_dict, no_need_label_set, no_need_lable_for_candidate, no_need_lable_node_dict, file_name, vailed_count=2):
    #相似的标1，不相似的标0，无法判断标2  语句不通顺情况下：标3
    #no_need_label_set = set()
    #no_need_lable_node_dict = dict() 
    with open(file_name) as fr:
        for line in fr:
            sps = line.split("\t")
            if len(sps) == 5:
                tree_q = util.clean_without_space(sps[0].strip()).encode("utf-8")
                biaozhu_q = util.clean_without_space(sps[1].strip())
                biaozhu_1 = sps[2].strip().replace(" ", "")
                biaozhu_2 = sps[3].strip().replace(" ", "")
                biaozhu_3 = sps[4].strip().replace(" ", "")
                key = is_vaild_biaozhu([biaozhu_1, biaozhu_2, biaozhu_3])
                if key == "1" or key == "2" or key == "3" or (not key):
                    no_need_label_set.add(biaozhu_q)
                    #no_need_lable_for_candidate.add(biaozhu_q)
                elif key == "0":
                    if tree_q in q2n_dict: 
                        node = q2n_dict[tree_q]
                        if node not in no_need_lable_node_dict:
                            no_need_lable_node_dict[node] = set() 
                        no_need_lable_node_dict[node].add(biaozhu_q)
                no_need_label_set.add(tree_q)
                #no_need_lable_for_candidate.add(tree_q)

def filter(file_name, q2n_dict, no_need_label_set, no_need_lable_for_candidate,no_need_lable_node_dict):
    for q, node, q_tmp in filter_file_load.load_file(file_name):
        if q in no_need_label_set:
            q = util.clean_without_space(q).encode("utf-8")
            continue
        res_list = [q, node]
        for candi_q in q_tmp:
            candi_q = util.clean_without_space(candi_q).encode("utf-8")
            if candi_q in no_need_lable_for_candidate:
                continue
            if node in no_need_lable_node_dict:
                if candi_q in no_need_lable_node_dict[node] or candi_q in no_need_lable_for_candidate:
                    continue
            res_list.append(candi_q)

        if len(res_list) > 2:
            print "\t".join(res_list)
        else:
            print >>sys.stderr, "res list 0：%s\t%s" % (q, node) 

                 

if __name__ == '__main__':
    file_db_path = sys.argv[1]
    q_n_file = sys.argv[2]
    to_be_filter_name = sys.argv[3]
    file_list = get_file_list(file_db_path)
    no_need_label_set = set()
    no_need_lable_for_candidate = set()
    q2n_dict = load_query_node(q_n_file, no_need_lable_for_candidate) 
    no_need_lable_node_dict = dict() 
    

    for file_name in file_list:
        if file_name not in parse_file_dict: 
            default_parse_handle(q2n_dict, no_need_label_set, no_need_lable_for_candidate, no_need_lable_node_dict, file_name, vailed_count=2)


    filter(to_be_filter_name, q2n_dict, no_need_label_set, no_need_lable_for_candidate, no_need_lable_node_dict)


    
