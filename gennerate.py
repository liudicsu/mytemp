#coding=utf-8
import sys

def load_node_num(file_name):
    node_num_dict = {}
    with open(file_name) as fr:
        for line in fr:
            line = line.strip()
            sps = line.split("\t")
            if sps[1].strip() not in node_num_dict:
                node_num_dict[sps[1].strip()] = 0
            node_num_dict[sps[1].strip()] += int(sps[0])
    return node_num_dict

def load_biaozhu(file_name):
    data_list = []
    with open(file_name) as fr:
        for line in fr:
            sps = line.strip().split("\t")
            data_list.append(sps)
    return data_list

def sort_with_num(data_list, node_num_dict):
    result_list = []
    for data in data_list: 
        node = data[1].strip() 
        node_num = 0
        if node in node_num_dict:
            node_num = node_num_dict[node]

        result_list.append((node_num, data))
    result_list = sorted(result_list, key=lambda i:i[0])
    return result_list

def print_result(result_list):
    No = 0
    for node_num, data in result_list:
        No += 1 
        query = data[0].strip()
        node = data[1].strip()
        q_list = data[2:12]
        if node_num < 7:
            print "%s\t%s\t%s\t%s" % (No, query, node, node_num)
            print "%s\t\t\t" % No,
            join_str = "\n%s\t\t\t" % No
            print join_str.join(q_list)

if __name__ == "__main__":
    data_list_file = sys.argv[1]
    node_num_dict_file = sys.argv[2] 
    data_list = load_biaozhu(data_list_file)
    node_num_dict = load_node_num(node_num_dict_file)
    result_list = sort_with_num(data_list, node_num_dict)
    print "ID\tquery\t节点\t节点数" #% (No, query, node, node_num)
    print_result(result_list)
