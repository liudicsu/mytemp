#coding=utf-8
import sys
import numpy as np
import json
import faiss
from get_sim_util_for_bert import *
m_dim = 768*4

class FaissIndex(object):         
    def __init__(self, m_dim, n_probe = 20, n_list = 200):
        quantizer  = faiss.IndexFlatL2(m_dim)
        self.index = faiss.IndexIVFFlat(quantizer, m_dim, n_list, faiss.METRIC_INNER_PRODUCT)
        self.index.nprobe = n_probe
    
    def train(self, num_dim_data, useAll=True, sample_ratio = 0.5):     
        if useAll:
            print >>sys.stderr, type(num_dim_data)
            print >>sys.stderr, num_dim_data.shape
            print >>sys.stderr, num_dim_data
            self.index.train(num_dim_data)
        else:
            self.index.train(num_dim_data)

    def add(self, num_dim_data):
        self.index.add(num_dim_data)

    def search(self, num_dim_data, return_k = 100): 
        D, I =   self.index.search(num_dim_data, return_k)     
        return I, D

def simple_fais(argv):
    db_file   =   argv[1]
    test_file =   argv[2]  

    db_querys, db_nodes= [],[]
    db_embedings = load_file(db_file, db_querys, db_nodes)

    db_querys = np.array(db_querys)
    db_nodes= np.array(db_nodes)


    test_querys, test_nodes = [],[]
    test_embedings = load_file(test_file, test_querys, test_nodes)

    test_querys =  np.array(test_querys) 
    test_nodes = np.array(test_nodes)

    faiss_index = FaissIndex(m_dim)
    faiss_index.train(db_embedings)
    faiss_index.add(db_embedings) 

    index_sort, mat_result = faiss_index.search(test_embedings) 
    save_pickle_faiss(db_querys, test_querys, db_nodes, test_nodes, mat_result, index_sort, test_file+"_top10_bert.pickle") 


if __name__ == "__main__":
    simple_fais(sys.argv)
