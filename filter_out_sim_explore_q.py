# -*- coding: utf-8 -*-
import patterns
import sys
pattern_tool = patterns.Patterns()

def jaccard(set1, set2):
    return float(len(set1 & set2)) / len(set1.union(set2))
    
def _process_eqs(datas, last_q):
    output_datas = []
    selected_features = []
    features = set()
    #for pattern_, slot_feas in pattern_tool.iter_patterns(last_q, max_slot_num=1, max_window_length=4):
    #    features.add(slot_feas.values()[0])
    #selected_features.append(features)
    for each in datas:#[::-1]:
        eq = each[-1]
        remain = True
        if len(selected_features) < 4:
            features = set()
            for pattern_, slot_feas in pattern_tool.iter_patterns(eq, max_slot_num=1, max_window_length=4):
                features.add(slot_feas.values()[0])
            
            if len(features) > 0:
                for each_selected_fea in selected_features:
                    s_ = jaccard(each_selected_fea, features)
                    if s_ > 0.3:
                        remain = False
                        break
        else:
            remain = False
        if remain:
            if len(features) > 0:
                selected_features.append(features)
            each.extend('1')
        else:
            each.extend('0')
        output_datas.append(each)
        
    return output_datas
    
def filter_out_anyq():
    wfile1=sys.argv[1]
    wfile2=sys.argv[2]
    rfile=sys.argv[3]
    current_id = '0'
    datas = []
    remain_num = 0
    last_q = ''
    #with open('daibiaozhu/after_jcad.1', 'w') as ofile2:
    with open(wfile1, 'w') as ofile2:
        #with open('daibiaozhu/after_jcad.2', 'w') as ofile:
        with open(wfile2, 'w') as ofile:
            #with open('daibiaozhu/after_jcad') as ifile:
            with open(rfile) as ifile:
                print >>ofile2, "Q1\tQ2\t是否予以相同"
                for idx, line in enumerate(ifile):
                    line = line.strip()
                    if idx == 0:
                        print >> ofile, line + '\t' + '过滤'
                        continue
                    toks = line.split('\t')
                    if len(toks) != 4:
                        continue
                    id_, q, node, q_ = toks
                    if id_ != current_id:
                        output_datas = _process_eqs(datas, last_q)
                        for each in output_datas:
                            if each[-1] == '1':
                                remain_num += 1
                                print >> ofile2, '\t'.join([last_q, each[3]])
                            print >> ofile, '\t'.join(each)
                        datas = []
                        current_id = id_
                        last_q = q
                        print >> ofile, line + '\t1'
                    else:
                        datas.append(toks)
            output_datas = _process_eqs(datas, last_q)
            for each in output_datas:
                if each[-1] == '1':
                    remain_num += 1
                    print >> ofile2, '\t'.join([last_q, each[3]])
                print >> ofile, '\t'.join(each)
    print 'remain num:%d'%remain_num
            
if __name__ == '__main__':
    filter_out_anyq()
