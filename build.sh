#cat all_node_qa_faq.txt |awk -F "\t" 'BEGIN{print "question\tanswer"}{print $2"\t"$1}' > .t
#cat node_query_11-13 |awk -F "\t" 'BEGIN{print "question\tanswer"}{print $3"\t"$2}' > .t
cat all_data_train |awk -F "\t" 'BEGIN{print "question\tanswer"}{print $1"\t"$1}' > .t
cp -rf .t ~/AnyQ/build/
cd ~/AnyQ/build/
cat >example/conf/dict.conf<<EOF
name: "example_dict_conf"
    
dict_config {
    name: "rank_weights"
    type: "String2FloatAdapter"
    path: "./rank_weights"
}

dict_config {
    name: "lac"
    type: "WordsegAdapter"
    path: "./wordseg_utf8"
}

dict_config{
    name: "fluid_simnet"
    type: "PaddleSimAdapter"
    path: "./simnet"
}

EOF
mkdir -p faq
python solr_script/make_json.py .t faq/schema_format faq/faq_json
awk -F "\t" '{print ++ind"\t"$0}' faq/faq_json > faq/faq_json.index
./annoy_index_build_tool example/conf/ example/conf/analysis.conf faq/faq_json.index 128 10 semantic.annoy  1>std 2>err
cp faq/faq_json.index semantic.annoy example/conf

cat >example/conf/dict.conf<<EOF
name: "example_dict_conf"

dict_config {
    name: "rank_weights"
    type: "String2FloatAdapter"
    path: "./rank_weights"
}

dict_config {
    name: "lac"
    type: "WordsegAdapter"
    path: "./wordseg_utf8"
}

dict_config{
    name: "fluid_simnet"
    type: "PaddleSimAdapter"
    path: "./simnet"
}

dict_config {
    name: "annoy_knowledge_dict"
    type: "String2RetrievalItemAdapter"
    path: "./faq_json.index"
}
EOF
