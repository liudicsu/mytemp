#coding=utf-8
import sys
import jieba.posseg as pseg
import jieba
import filter_file_load
import os

s_set = set()
if not os.getenv("INMAC"):
    jieba.load_userdict("daibiaozhu/dict.ori.txt")
    print >>sys.stderr, "load dict done"

pos_list = set(["nr",  "nr1", "nr2", "nrj", "nrf", "ns", "nsf", "nt", "nz"])
pos_list_add_n = pos_list | set(["x"])

def load_stop_word(filename = "stop.txt"):
    stop_set = set()
    with open(filename) as fr:
        for line in fr:
            stop_set.add(line.strip().decode("utf-8"))
    return stop_set

g_stop_set = load_stop_word()

#def filter_stop(seg_list, stop_set=g_stop_set):
#    return [i for i in seg_list if i not in stop_set]


def cut(q):
    words = list(pseg.cut(q))
    q_pos_n = set(w.word.encode("utf-8") for w in words if (w.flag in pos_list and w.word not in g_stop_set))
    q_pos_others = list(w.word.encode("utf-8") for w in words if (w.flag not in pos_list_add_n and w.word not in g_stop_set))
    q = "".join(w.word.encode("utf-8") for w in words if  w.word not in g_stop_set)
    return q_pos_n, q_pos_others, q

def match_two(q_pos_n, q_pos_others, candi_q_pos_n, candi_q_pos_others, q, candi_q):
    tmp = q_pos_n ^ candi_q_pos_n 
    if len(tmp) == 0:
        return candi_q
    elif len(q_pos_n) == 0: 
        #return "".join(candi_q_pos_others)
        return None
    else:
        return None 
    
def filter(file_name):
    for q, node, q_tmp in filter_file_load.load_file(file_name):
        q_pos_n, q_pos_others, q_1 = cut(q)
        if q in s_set:
            continue
        if q_1 in s_set:
            continue
        s_set.add(q) 
        s_set.add(q_1)
        res_list = [q, node]

        for candi_q in q_tmp:
            candi_q_pos_n, candi_q_pos_others, candi_q_1 = cut(candi_q)
            if candi_q in s_set:
                continue
            if candi_q_1 in s_set:
                continue
             
            res = match_two(q_pos_n, q_pos_others, candi_q_pos_n, candi_q_pos_others, q, candi_q)
            if res is None: 
                continue    
            if res in s_set:
                continue
            s_set.add(candi_q)
            s_set.add(candi_q_1)
            s_set.add(res)
            res_list.append(res)

        if len(res_list) > 2:
            print "\t".join(res_list)
        else:
            print >>sys.stderr, "res list 0：%s\t%s" % (q, node) 

if __name__ == "__main__":
    file_name = sys.argv[1] 
    filter(file_name)
