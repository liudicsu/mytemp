#coding=utf-8
import sys
import pickle
import numpy as np
import util
import re



def load_qq_match_vector(file_name, all_query_file):
    data_dict = {}
    with open(file_name) as reader:
        with open(all_query_file) as all_query:
            for t_e, t_q in zip(reader, all_query): 
                sps = t_e.strip().split(" ")
                emb = [float(i) for i in sps[1:]]
                query = t_q.strip()
                data_dict[query] = emb
    return data_dict


def load_pickle(pickle_file):
    retrive_data = {}
    with open(pickle_file) as p:
        retrive_data = pickle.load(p)
    return retrive_data


def filter_one_qqm(key, values, emb_dict):
    if key not in emb_dict:  
        return None 
    key_emb = np.array(emb_dict[key])
    result_list = []
    for query, score in values:
        if query in emb_dict:
            q_emb = np.array(emb_dict[query])
            coss = q_emb.dot(key_emb)
            if coss > 0.86:
                continue
            result_list.append((query, score)) 
        else:
            print >>sys.stderr, "%s\t%s is too sim" % (key, query)
    return result_list


def filter_all(emb_dict, retrive_dict):
    result_dict = {} 
    for key in retrive_dict:
        if key not in result_dict:
            t = filter_one_qqm(key, retrive_dict[key], emb_dict) 
            if t is None:
                print >>sys.stderr, "not in embdict %s" % key
                continue
            result_dict[key] = t
    return result_dict 


def load_query_node(node_query_file):
    q2n_dict = {}
    with open(node_query_file) as nqf:
        for line in nqf:
            sps = line.strip().split("\t")
            query = sps[1]
            node = sps[0]
            query = util.clean_line_chinese_punch(query).encode("utf-8")
            q2n_dict[query] = node
    return q2n_dict

def print_result(q2n_dict, result_dict):
    for key in result_dict: 
        node = "None"
        tmp_set = set()
        tmp_set.add(key)
        if key in q2n_dict:     
            node = q2n_dict[key] 
        else:
            print >>sys.stderr, "not in q2n %s" % key
        result_list = [key, node]  
        for q, s in result_dict[key]:
            if q not in tmp_set: 
                tmp_set.add(q)
                result_list.append(q)
        if len(result_list) <= 2:
            print >>sys.stderr, "result list zero %s" % key
        print "\t".join(result_list[0:13])
            

if __name__ == "__main__":
    retrive_pickle_file = sys.argv[1] 
    qq_match_vector_file = sys.argv[2]
    all_query_file = sys.argv[3]
    node_query_file = sys.argv[4]
    print >>sys.stderr, "load qq match vector"
    emb_dict = load_qq_match_vector(qq_match_vector_file, all_query_file)
    print >>sys.stderr, "emb_dict len", len(emb_dict)

    print >>sys.stderr, "load pickle"
    retrive_data = load_pickle(retrive_pickle_file)
    print >>sys.stderr, "retrive len", len(retrive_data)

    
    print >>sys.stderr, "filter all"
    result_dict = filter_all(emb_dict, retrive_data)
    print >>sys.stderr, "resultdict len", len(result_dict)


    print >>sys.stderr, "load query node"
    q2n_dict = load_query_node(node_query_file) 
    print >>sys.stderr, "q2n len", len(q2n_dict)


    print >>sys.stderr, "print result"
    print_result(q2n_dict, result_dict)
