#coding=utf-8
from bert_serving.client import BertClient
import numpy as np
import sys
import time
if __name__ == "__main__":
	bc = BertClient()
	try:
		while True: 
			line_a = input("输入句子A:")
			line_b = input("输入句子B:")
			s_time = time.time()	
			array = bc.encode([line_a.strip(),line_b.strip()]*50)
			e_time = time.time()	
			print("used: %s" % ((e_time - s_time)*1000))
			norm_np = np.linalg.norm(array, axis=1)
			shape_p = norm_np.shape
			array = array/norm_np.reshape(shape_p[0],1)
			print(array.dot(array.transpose()))
	except Exception as e:
		print(e)
