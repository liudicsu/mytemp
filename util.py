#coding=utf-8

import re
import sys

chinese_punc=ur"[。……！？｡＂＃＄％＆＇（）＊＋，－／：；＜＝＞＠［＼］＾＿｀｛｜｝～｟｠｢｣､、〃》「」『』【】〔〕〖〗〘〙〚〛〜〝〞〟〰〾〿–—‘’‛“”„‟…‧﹏.!\"#$%&'()*+,-./:;<=>?@\[\]^_`{|}~]+"

def clean_line_chinese_punch(line):
    if type(line) is str:
        line = line.decode("utf-8") 
    r_line = re.sub(chinese_punc, " ", line, flags=re.U).strip()
    return r_line.strip()

def clean_line_chinese_punch_n_and_q(line):
    if type(line) is str:
        line = line.decode("utf-8") 
    sps = line.strip().split("\t") 
    q = re.sub(chinese_punc, " ", sps[1], flags=re.U).strip()
    n = sps[0].strip()
    return "%s\t%s" % (n, q)

def clean_without_space(line):
    line = clean_line_chinese_punch(line)
    return line.replace(" ", "").strip()

if __name__ == "__main__":
    for line in sys.stdin:
        #print clean_line_chinese_punch(line).encode("utf-8")
        try:
            print clean_line_chinese_punch_n_and_q(line).encode("utf-8")
        except Exception as e:
            print >>sys.stderr, "=" * 20
            print >>sys.stderr, e
            print >>sys.stderr, "error line", line

