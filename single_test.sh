
jsim=$1
ssim=$2
cat >~/AnyQ/build/example/conf/rank_weights<<EOF
jaccard_sim	$1
fluid_simnet_feature	$2
EOF
cat ~/AnyQ/build/example/conf/rank_weights
sh restart.sh

cat diff_file/data/old_node_new_query_sample |python trans.py >cmp/old_node.anyq.result
cat diff_file/data/added_node_query_sample   |python trans.py >cmp/new_node.anyq.result
cat cmp/old_node.anyq.result|python mean_score_and_mean_top.py

echo 
echo
