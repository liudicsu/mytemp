#coding=utf-8
import sys

if __name__ == "__main__":
    t_num = 0
    t_score = 0.0
    t_index = 0.0
    for line in sys.stdin:
        sps = line.strip().split("\t") 
        q = sps[0]
        ra = sps[1]
        for _i in  range(5,len(sps),4):
            if sps[_i] == "True":
                t_num += 1  
                t_score += float(sps[_i-2]) 
                t_index += (_i-5)/4
    print "total num %s" % t_num
    print "mean score %s" % (t_score/t_num)
    print "mean index %s" % (t_index/t_num)
    
