#coding=utf-8
import pickle
import sys

def load_file_list(list_file):
    file_list = []
    with open(list_file) as l_f:
        for line in l_f:
            file_list.append(line.strip())
    return file_list


def load_pickle(pickle_file):
    data = {}
    with open(pickle_file) as file_r: 
        data = pickle.load(file_r)
    return data


def merge_data(data_1, data_2):
    if data_1 == {}:
        return data_2 

    if data_2 == {}:
        return data_1

    total_data = {}
    for key in data_1:
        d_1 = data_1[key]
        if key in data_2: 
            d_1.extend(data_2[key])
        d_1 = sorted(d_1, key=lambda i:i[1], reverse=True)[:100]
        total_data[key] = d_1
    return total_data



def merge_main(list_file_name):
    file_list = load_file_list(list_file_name)
    total_data = {} 
    for f_n in file_list:
        tmp_data = load_pickle(f_n)
        total_data = merge_data(total_data, tmp_data)
    return total_data  

def print_all_query(total_data):
    for key in total_data:
        print key
        for item in total_data[key]:
            print item[0]

if __name__ == "__main__":
    total_data = merge_main(sys.argv[1])
    save_file = sys.argv[2] 
    with open(save_file, "w") as s_f:
        pickle.dump(total_data, s_f) 
    print_all_query(total_data)
