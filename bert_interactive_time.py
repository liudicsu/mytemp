#coding=utf-8
from bert_serving.client import BertClient
import bert_serving
print(bert_serving.__file__)
import numpy as np
import sys
import time
input_line = "我是刘迪你是谁快点告诉他妈的去你去死快点跌"
if __name__ == "__main__":
    bc = BertClient()
    total = 0.0
    try:
        for i in range(0, 1000):
            s_time = time.time()
            array = bc.encode([input_line])
            e_time = time.time()
            print("single time %s\n " % ((e_time - s_time)*1000))
            total += e_time - s_time
        print("mean time %s" % total)
    except Exception as e:
        print(e)
