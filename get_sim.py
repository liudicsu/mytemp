#coding=utf-8
import sys
import numpy as np
import json
m_dim = 128

def json_query(line):
    j_data = json.loads(line) 
    return j_data["question"].strip().encode('utf-8'), j_data["answer"].strip().encode('utf-8')

def load_file(filename):
    embedings = []
    querys = []
    nodes = []
    with open(filename) as reader:
        for line in reader:
            sps = line.strip().split("\t") 
            if len(sps) != m_dim +1:
                continue
            query, node = json_query(sps[0].strip())
            querys.append(query)
            nodes.append(node)
            embedings.append([float(i) for i in sps[1:]]) 
    embedings = np.array(embedings).astype('float32') 
    norm_np = np.linalg.norm(embedings, axis=1) 
    shape_p = norm_np.shape 
    embedings = embedings/norm_np.reshape(shape_p[0],1)
    np.nan_to_num(embedings, False)
    print embedings
    return np.array(querys), embedings, np.array(nodes)


def compute(db_embedings, test_embedings):
    mat_result = db_embedings.dot(test_embedings.transpose())
    index_sort = np.argsort(-mat_result, axis=0)
    return index_sort.transpose(), mat_result.transpose()

def print_result(db_querys, test_querys, db_nodes, test_nodes, mat_result, index_sort):
    for t_q, m_r, i_s, t_n in zip(test_querys, mat_result, index_sort, test_nodes):
        tmp = []
        tmp.append("ANYQ\t"+t_q+"\t"+t_n)  
        tmp.extend(i[0]+":"+i[1]+"\t"+i[2] for i in np.array([m_r[i_s], db_querys[i_s], db_nodes[i_s]] ).transpose()[0:5])
        print "\t".join(tmp) 

def save_pickle(db_querys, test_querys, db_nodes, test_nodes, mat_result, index_sort, save_file):
    result_dict = {}
    for t_q, m_r, i_s, t_n in zip(test_querys, mat_result, index_sort, test_nodes):
        tmp_tuple_list = []
        #query score same tuple
        tmp_tuple_list.extend((i[1], float(i[0]), i[2] == t_n) for i in np.array([m_r[i_s], db_querys[i_s], db_nodes[i_s]]).transpose()[0:10] if float(i[0]) > 0.8 and t_q != i[1])
        result_dict[t_q] = tmp_tuple_list

        tmp = []
        tmp.append("ANYQ\t"+t_q+"\t"+t_n)  
        tmp.extend(i[0]+":"+i[1]+":"+str(i[2]==t_n) for i in np.array([m_r[i_s], db_querys[i_s], db_nodes[i_s]] ).transpose()[0:10] if float(i[0]) > 0.8 and t_q != i[1])
        print "\t".join(tmp) 
    with open(save_file, "wb") as writer:
        import pickle
        pickle.dump(result_dict, writer)


def save_pickle_faiss(db_querys, test_querys, db_nodes, test_nodes, mat_result, index_sort, save_file):
    result_dict = {}
    for t_q, m_r, i_s, t_n in zip(test_querys, mat_result, index_sort, test_nodes):
        tmp_tuple_list = []
        #query score same tuple
        tmp_tuple_list.extend((i[1], float(i[0]), i[2] == t_n) for i in np.array([m_r, db_querys[i_s], db_nodes[i_s]]).transpose()[0:10] if float(i[0]) > 0.8 and t_q != i[1])
        result_dict[t_q] = tmp_tuple_list

        tmp = []
        tmp.append("ANYQ\t"+t_q+"\t"+t_n)  
        tmp.extend(i[0]+":"+i[1]+":"+str(i[2]==t_n) for i in np.array([m_r, db_querys[i_s], db_nodes[i_s]] ).transpose()[0:10] if float(i[0]) > 0.8 and t_q != i[1])
        print "\t".join(tmp) 
    with open(save_file, "wb") as writer:
        import pickle
        pickle.dump(result_dict, writer)

         

if __name__ == "__main__":
    db_file = sys.argv[1]
    test_file = sys.argv[2]  
    db_querys, db_embedings, db_nodes= load_file(db_file)
    test_querys, test_embedings, test_nodes = load_file(test_file)
    index_sort, mat_result = compute(db_embedings, test_embedings)
    save_pickle(db_querys, test_querys, db_nodes, test_nodes, mat_result, index_sort, test_file+"_top10.pickle")
