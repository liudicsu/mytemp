#coding=utf-8
import pickle
import sys

def load_data(data_file):
    data_dict = None
    with open(data_file, "rb") as reader:
        data_dict = pickle.load(reader)
    return data_dict

def singe_data(thresold, q, a_list):
    is_ok = True
    finded = False 
    for a_i, a_s, a_flag in a_list:
        if float(a_s) >= thresold and a_flag:
            finded = True
        elif float(a_s) >= thresold and not a_flag:
            is_ok = False
            break
        else:
            break
    return is_ok, finded


def static_for_given_threshold(threshold, data_dict):
    total_num = len(data_dict)
    total_ok_num  = 0
    finded_num = 0 
    total_not_ok = 0
    #total_not_ok = 0
    for q, a_list in data_dict.iteritems(): 
        is_ok, finded = singe_data(threshold, q, a_list) 
        if is_ok:
            total_ok_num += 1
            if finded:
                finded_num += 1
        else:
            total_not_ok += 1
    return total_num, total_ok_num, finded_num, total_not_ok


def get_result(total_num, total_ok_num, finded_num, total_not_ok, threshold):
    total_num_col = "总数"
    threthreshold_col = "阈值"
    finded_percent_col = "召回率"
    pre_col = "正确率"
    wrong_pre_col = "误召率"
    finded_percent =  (finded_num*1.0) / total_num
    pre =  (total_ok_num*1.0) / total_num
    wrong_pre =  ((total_not_ok)*1.0) / total_num
    
    finded_num_col = "召回个数"
    total_ok_num_col = "正确个数"

    local_vars = locals()
    #print local_vars
    total_str = "{threthreshold_col:<20}:{threshold}\n"\
                "{total_num_col:<20}:{total_num}\n"\
                "{finded_num_col:<20}:{finded_num}\n"\
                "{total_ok_num_col:<20}:{total_ok_num}\n"\
                "{finded_percent_col:<20}:{finded_percent}\n"\
                "{pre_col:<20}:{pre}\n"\
                "{wrong_pre_col:<20}:{wrong_pre}\n".format(**local_vars)
    return total_str
    

if __name__ == "__main__":
    file_name = sys.argv[1]
    data_dict = load_data(file_name) 
    for i in range(80, 100, 1):
        i = i/100.0
        print "=" * 10
        print get_result(*static_for_given_threshold(i, data_dict), threshold=i)

