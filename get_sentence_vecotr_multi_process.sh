fold_prefix=$2
first_step_file=$3
mkdir -p $fold_prefix

if [[ "${first_step_file}x" == "x" ]];then
	cat $1 |awk -v fold_prefix=$fold_prefix -F "\t" 'BEGIN{print "question\tanswer">> fold_prefix"/0.origin"}{file=int(NR / 500000);file=file".origin"; mod=NR % 500000;if(mod == 0){print "question\tanswer">> fold_prefix"/"file};print >>fold_prefix"/"file}' 
fi

if [[ "${first_step_file}x" != "x" ]];then

	cat $1|sh $first_step_file |awk -v fold_prefix=$fold_prefix -F "\t" 'BEGIN{print "question\tanswer">> fold_prefix"/0.origin"}{file=int(NR / 500000); file=file".origin";mod=NR % 500000;if(mod == 0){print "question\tanswer">> fold_prefix"/"file};print >>fold_prefix"/"file}' 
	#cat $1|sh $2 |awk -F "\t" 'BEGIN{print "question\tanswer"}{print }' > .t
fi

fold_name=`readlink -f  $fold_prefix`
cd ~/AnyQ/build/
find $fold_name -name "*.origin"|xargs -n 1 -P 36 -I {}  python solr_script/make_json.py {} faq/schema_format {}".tmpjson" 
find $fold_name -name "*.tmpjson"| xargs -n 1 -P 36 -I {} ./a_too example/conf/ example/conf/analysis.conf  {}  128  {}".vector" 
#result=`readlink -f result`
#cd -
#mv $result ${1}".vector"
