#coding=utf-8
import sys
import numpy as np
import json
import faiss
from get_sim_util_for_faiss import *
m_dim = 128

class FaissIndex(object):         
    def __init__(self, m_dim, n_probe = 20, n_list = 200):
        quantizer  = faiss.IndexFlatL2(m_dim)
        self.index = faiss.IndexIVFFlat(quantizer, m_dim, n_list, faiss.METRIC_INNER_PRODUCT)
        self.index.nprobe = n_probe
    
    def train(self, num_dim_data, useAll=True, sample_ratio = 0.5):     
        if useAll:
            self.index.train(num_dim_data)
        else:
            self.index.train(num_dim_data)

    def add(self, num_dim_data):
        self.index.add(num_dim_data)

    def search(self, num_dim_data, return_k = 100): 
        D, I =   self.index.search(num_dim_data, return_k)     
        return I, D

def simple_fais(argv):
    db_file   =   argv[1]
    test_file =   argv[2]  

    db_querys, db_nodes= [],[]
    db_embedings = load_file(db_file, db_querys, db_nodes)

    db_querys = np.array(db_querys)
    db_nodes= np.array(db_nodes)


    test_querys, test_nodes = [],[]
    test_embedings = load_file(test_file, test_querys, test_nodes)

    test_querys =  np.array(test_querys) 
    test_nodes = np.array(test_nodes)

    faiss_index = FaissIndex(128)
    faiss_index.train(db_embedings)
    faiss_index.add(db_embedings) 

    index_sort, mat_result = faiss_index.search(test_embedings) 
    save_pickle_faiss(db_querys, test_querys, test_nodes, mat_result, index_sort, test_file+"_top10_faiss.pickle") 

def huge_fais(argv):
    train_file = argv[1]
    db_list_file   =   argv[2]
    test_file =   argv[3]  

    faiss_index = FaissIndex(128)

    train_querys, train_nodes= [],[]
    train_embedings = load_file(train_file, train_querys, train_nodes, False, False)
    faiss_index.train(train_embedings)
    del train_embedings 

    file_list = []
    with open(db_list_file) as list_reader:
        for line in list_reader:
            file_list.append(line.strip()) 

    db_querys, db_nodes= [],[]
    for db_file in file_list: 
        db_embedings = load_file(db_file, db_querys, db_nodes, need_query=True, need_node=False)
        faiss_index.add(db_embedings) 
        del db_embedings

    db_querys = np.array(db_querys)


    test_querys, test_nodes = [],[]
    test_embedings = load_file(test_file, test_querys, test_nodes)

    test_querys =  np.array(test_querys) 
    test_nodes = np.array(test_nodes)

    index_sort, mat_result = faiss_index.search(test_embedings) 
    save_pickle_faiss(db_querys, test_querys, test_nodes, mat_result, index_sort, test_file+"_top10_faiss.pickle") 

def multi_simpy_faiss_main():
    db_list_file = sys.argv[1]
    test_file = sys.argv[2]
    file_list = []
    with open(db_list_file) as list_reader:
        for line in list_reader:
            file_list.append(line.strip()) 

    test_querys, test_nodes = [],[]
    test_embedings = load_file(test_file, test_querys, test_nodes)

    test_querys =  np.array(test_querys) 
    test_nodes = np.array(test_nodes)

    for index, db_file in enumerate(file_list):
        try:
            multi_simple_fais(test_file, db_file, test_querys, test_embedings, test_nodes, index)
        except Exception as e:
            print >>sys.stderr, e



def multi_simple_fais(test_file, db_file, test_querys, test_embedings, test_nodes, index):
    db_querys, db_nodes= [],[]
    db_embedings = load_file(db_file, db_querys, db_nodes, need_query=True, need_node=False)
    db_querys = np.array(db_querys)
    db_nodes= np.array(db_nodes)

    faiss_index = FaissIndex(128)
    faiss_index.train(db_embedings)
    faiss_index.add(db_embedings) 
    index_sort, mat_result = faiss_index.search(test_embedings) 
    save_pickle_faiss(db_querys, test_querys, test_nodes, mat_result, index_sort, test_file+str(index)+".step1pickle") 

if __name__ == "__main__":
    multi_simpy_faiss_main()
