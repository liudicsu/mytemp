#coding=utf-8

import re
import sys

chinese_punc=ur"[。……！？｡＂＃＄％＆＇（）＊＋，－／：；＜＝＞＠［＼］＾＿｀｛｜｝～｟｠｢｣､、〃》「」『』【】〔〕〖〗〘〙〚〛〜〝〞〟〰〾〿–—‘’‛“”„‟…‧﹏.!\"#$%&'()*+,-./:;<=>?@\[\]^_`{|}~]+"

def clean_line_chinese_punch(line):
    if type(line) is str:
        line = line.decode("utf-8") 
    r_line = re.sub(chinese_punc, " ", line, flags=re.U).strip()
    return r_line.strip()

