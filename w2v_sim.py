#coding=utf-8
import sys
import numpy as np
import json

def json_query(line):
    j_data = json.loads(line) 
    return j_data["question"].encode('utf-8'), j_data["answer"].encode('utf-8')

def load_v2w_file(filename):
    embedings = []
    w2index = {}
    querys = []
    with open(filename) as reader:
        line = reader.readline()
        for index, line in enumerate(reader):
            sps = line.strip().split(" ") 
            word = sps[0]
            w2index[word] = index
            embedings.append([float(i) for i in sps[1:]]) 
            querys.append(word)
    norm_np = np.linalg.norm(np.array(embedings), axis=1) 
    shape_p = norm_np.shape 
    return np.array(querys), w2index, np.array(embedings)/norm_np.reshape(shape_p[0],1)


def compute(db_embedings, test_embedings):
    mat_result = db_embedings.dot(test_embedings.transpose())
    index_sort = np.argsort(-mat_result, axis=0)
    return index_sort.transpose(), mat_result.transpose()

def print_result(db_querys, test_querys,  mat_result, index_sort):
    for t_q, m_r, i_s in zip(test_querys, mat_result, index_sort):
        tmp = []
        tmp.append(t_q)  
        tmp.extend(i[1]+":"+i[0] for i in np.array([m_r[i_s], db_querys[i_s]]).transpose()[0:30])
        print "\n".join(tmp) 

def find_word(word, db_embedings, w2index):
    if word not in w2index:
        raise Exception("%s not in dict" % word) 
    word_index = w2index[word]
    return db_embedings[[word_index]], np.array([word])

def comput_two_word_similary(db_embedings, db_querys, w2index):
    w_pair = raw_input("word pair word1|word2:") 
    sps = w_pair.strip().split("|")
    if len(sps) != 2:
        print >>sys.stderr, "ERROR, input word pair: word1|word2"
    word1 = sps[0]
    word2 = sps[1]
    word1_db_embedings, word1 = find_word(word1, db_embedings, w2index) 
    word2_db_embedings, word2 = find_word(word2, db_embedings, w2index) 
    index_sort, mat_result = compute(word2_db_embedings, word1_db_embedings)
    print_result(word1, word2,  mat_result, index_sort)
    
def find_most_similar(db_embedings, db_querys, w2index):
    word = raw_input("word:") 
    word_db_embedings, word = find_word(word.strip(), db_embedings, w2index) 
    index_sort, mat_result = compute(db_embedings, word_db_embedings)
    print_result(db_querys, word,  mat_result, index_sort)


if __name__ == "__main__":
    if  len(sys.argv) != 3:
        print >>sys.stderr, "params not enough" 
        print >>sys.stderr, "python %s (pair|most)  db_file" % sys.argv[0] 
        sys.exit(-1)
    find_mode = sys.argv[1]
    db_file = sys.argv[2]  

    if find_mode == "pair":
        handler = comput_two_word_similary
    elif find_mode == "most":
        handler = find_most_similar
    else:
        print >>sys.stderr, "mode %s is not support" % find_mode
        print >>sys.stderr, "python %s (pair|most)  db_file" % sys.argv[0] 
        sys.exit(-1)

    db_querys, w2index, db_embedings = load_v2w_file(db_file)

    while True:
        try:  
            handler(db_embedings, db_querys, w2index)
        except Exception as e:
            #import traceback
            #print >>sys.stderr, traceback.format_exc()
            print >>sys.stderr, e
