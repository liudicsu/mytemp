#coding=utf-8

import sys
import json
import urllib
import urllib2
url_template = "{}?{}"

def get_request(url, param):
    url_param = urllib.urlencode(param)
    full_url = url_template.format(url, url_param) 
    return urllib2.urlopen(full_url).read()
    
if __name__ == "__main__":
    for line in sys.stdin:
        sps = line.strip().split("\t")
        q = sps[0]
        a = u"None"
        if len(sps) >=2:
            a = sps[1]
        param = {"question": q}
        
        #str_value = get_request("http://127.0.0.1:8901/anyq", param)
        str_value = get_request("http://dfe388d6f506:8901/anyq", param)
        json_value = json.loads(str_value)
        tmp_list = [q.decode("utf-8")+"\t"+a.decode("utf-8")]
        for item in json_value[0:20]:
            if line.strip().decode("utf-8") != item["question"].strip():
                tmp_list.append(item["question"]+"\t"+str(item["confidence"])+"\t"+item["answer"]+"\t"+str(a.decode("utf-8") == item["answer"]))
        print "\t".join(tmp_list[0:6]).encode("utf-8")

