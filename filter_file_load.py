#coding=utf-8

all_s = set()


def load_file(file_name):
    with open(file_name) as file_r:
        for line in file_r:
            sps = line.strip().split("\t")        
            if len(sps) <= 2:
                continue
            q = sps[0].replace(" ", "")          
            if q in all_s:
                continue
            else:
                all_s.add(q)

            node = sps[1]
            q_list = [i.replace(" ", "") for i in sps[2:]] 
            q_tmp = []
            for i in q_list: 
                if i not in all_s:
                    all_s.add(i) 
                    q_tmp.append(i)
            if len(q_tmp) == 0:
                continue
            yield q, node, q_tmp
    all_s.clear() 
            
