
cat > ~/AnyQ/build/example/conf/retrieval.conf <<EOF
retrieval_plugin {
    name : "semantic_recall"
    type : "SemanticRetrievalPlugin"
    vector_size : 128
    search_k : 10000 
    index_path : "./example/conf/semantic.annoy"
    using_dict_name: "annoy_knowledge_dict"
    num_result : $1
}
EOF
echo return num $1
echo ====================

cat >~/AnyQ/build/example/conf/rank_weights<<EOF
jaccard_sim	0.0
fluid_simnet_feature	1.0
EOF
cat ~/AnyQ/build/example/conf/rank_weights
sh restart.sh

