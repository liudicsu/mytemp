# -*- coding: utf-8 -*-
from collections import defaultdict
import itertools
#import utility
import math
import re
import random

MAX_SLOT_NUM = 3
MAX_WINDOW_LENGTH = 15
SLOT_PATTERN = r'(\[[\d\D]+?\])'
#def is_number():
    

class Pattern(object):
    def __init__(self, s, score):
        self.pattern = s
        self.score = float(score)
        rst =  re.search(SLOT_PATTERN, s)
        self.slot_distribution = {}
        self.examples = defaultdict(list)
        while rst is not None:
            slot_name = rst.group(1)
            self.slot_distribution[slot_name] = defaultdict(int)
            s = re.sub(SLOT_PATTERN, '', s, count=1)
            rst = re.search(SLOT_PATTERN, s)
        
    def match_and_add_to_distribution(self, q, slot_features, entities):
        all_slot_matched = True
        mapping = defaultdict(set)
        for slot_fea, slot_value in slot_features.iteritems():
            if slot_fea in self.slot_distribution and slot_value in entities:
                for slot_name in entities[slot_value]:
                    self.slot_distribution[slot_fea][slot_name] += 1
                    mapping[slot_fea].add((slot_fea, '[%s]'%slot_name))
            else:
                all_slot_matched = False
                
        if all_slot_matched:
            for combination in itertools.product(*mapping.values()):
                if len(self.examples[combination]) < 20:
                    self.examples[combination].append(q)
            self.score += 1
        else:
            self.score -= 0.1
            
    def get_patterns_with_slot_info(self):
        rst = []
        for mapping, examples in self.examples.iteritems():
            pattern_ = self.pattern
            replacings = set()
            for replaced, replacing in mapping:
                pattern_ = pattern_.replace(replaced, replacing)
                replacings.add(replacing)
            #[singer][singer] -> [singer]
            for replacing in replacings:
                for idx_ in xrange(MAX_SLOT_NUM, 1, -1):
                    pattern_ = pattern_.replace(replacing * idx_, replacing)
            rst.append((pattern_, random.sample(examples, min(3, len(examples)))))
        return rst
    
class Patterns(object):
    def __init__(self):
        pass
    
    def _build_query_characters(self, q):
        characters = []
        cache = []
        if type(q) is not unicode:
            q = q.decode('utf8')
        for each in q:
            each = each.encode("utf-8")
            current_is_alpha_or_number = each.isalpha() or each.isdigit()
            if current_is_alpha_or_number:
                cache.append(each)
            else:
                if len(cache) > 0:
                    characters.append(''.join(cache))
                characters.append(each)
                cache = []
            
        if len(cache) > 0:
            characters.append(''.join(cache))
        return characters
    
    def iter_patterns(self, q, max_slot_num=MAX_SLOT_NUM, max_window_length=MAX_WINDOW_LENGTH):
        characters = self._build_query_characters(q)
        characters_len = len(characters)
        if characters_len > 20:
            #考虑性能，太长的q放弃用模板方式
            return
        #最多2个槽位
        for slot_num in xrange(1, max_slot_num + 1):
            windows_lengths = []
            for _ in xrange(slot_num):
                windows_lengths.append(list(range(1, min(max_window_length + 1, characters_len - slot_num + 2))))
            for windows_length in itertools.product(*windows_lengths):
                if sum(windows_length) > characters_len:
                    continue
                for start_positions in itertools.combinations(range(characters_len - windows_length[-1] + 1), slot_num):
                    #check
                    is_valid = True
                    for idx_ in xrange(slot_num - 1):
                        if start_positions[idx_] + windows_length[idx_] > start_positions[idx_ + 1]:
                            is_valid = False
                            break
                    start_positions_set = {}
                    for idx_, p in enumerate(start_positions):
                        start_positions_set[p] = idx_
                    if is_valid:
                        pattern_tokens = []
                        slot_idx = 0
                        start_p = 0
                        slot_feas = {}
                        while start_p < characters_len:
                            if start_p in start_positions_set:
                                slot_fea = '[X%d]'%(slot_idx)
                                pattern_tokens.append(slot_fea)
                                slot_length = windows_length[start_positions_set[start_p]]
                                slot_feas[slot_fea] = ''.join(characters[start_p : start_p + slot_length])
                                slot_idx += 1
                                start_p += slot_length
                            else:
                                pattern_tokens.append(characters[start_p])
                                start_p += 1
                        pattern_ = ''.join(pattern_tokens)
                        yield pattern_, slot_feas
                        
    def mine_patterns(self, dataset):
        SLOT_MERGE = []
        for num_ in xrange(1, MAX_SLOT_NUM + 1):
            for start_ in xrange(0, MAX_SLOT_NUM - num_ + 1):
                replaced = []
                for i_ in xrange(start_, start_ + num_):
                    replaced.append('[X%d]'%i_)
                SLOT_MERGE.append((''.join(replaced), replaced[0]))
            
        pattern_freq = defaultdict(float)
        with open(dataset, 'r') as ifile:
            for idx, line in enumerate(ifile):
                if idx % 1000 == 0:
                    print 'processed %d querys, patterns(%d)'%(idx, len(pattern_freq))
                if idx >= 100000:
                    break
                toks = line.strip().split('\t')
                q = toks[1]
                #q = toks[0]
                patterns_ = []
                for pattern_, slot_features in self.iter_patterns(q):
                    patterns_.append(pattern_)
                weight = 1.0 / math.log(len(patterns_) + 2)
                for pattern_ in patterns_:
                    #for replaced, replacing in SLOT_MERGE:
                    #    pattern_ = pattern_.replace(replaced, replacing)
                    real_words = pattern_
                    for i_ in xrange(MAX_SLOT_NUM):
                        real_words = real_words.replace('[X%d]'%i_, '')
                    real_words_length = len(self._build_query_characters(real_words))
                    if real_words_length > 0:
                        score = math.log(real_words_length + 2)
                        pattern_freq[pattern_] += score ** 5 * weight
        print 'all pattern:%d'%(len(pattern_freq))
        average_score = sum(pattern_freq.values()) / len(pattern_freq)
        print 'average score:%.2f'%(average_score)
        with open('e:/share/mine_patterns', 'w') as ofile:
            for pattern_, score in pattern_freq.iteritems():
                if score >= average_score:
                    print >> ofile, '\t'.join([pattern_, '%.2f'%score])
        pattern_freq = []
        with open('e:/share/mine_patterns', 'r') as ifile:
            for line in ifile:
                pattern_, score = line.strip().split('\t')
                pattern_freq.append((pattern_, float(score)))
        print 'valid pattern:%d'%(len(pattern_freq))
        with open('e:/share/mine_patterns_sorted', 'w') as ofile:
            for pattern_, score in sorted(pattern_freq, key=lambda i:i[1], reverse=True)[:10000]:
                print >> ofile, '\t'.join([pattern_, '%.2f'%score])
        
    def load_entities(self):
        entities = defaultdict(set)
        with open('E:/数据集/music.utf8'.decode('utf8').encode('gbk'), 'r') as ifile:
            for line in ifile:
                toks = line.strip().split('\t')
                if len(toks) == 3:
                    entities[toks[0].strip().lower()].add('song')
                    entities[toks[1].strip().lower()].add('singer')
                    entities[toks[2].strip().lower()].add('album')
        with open('E:/py_nlu_data/data/music/MUSIC.tag', 'r') as ifile:
            for line in ifile:
                toks = line.strip().split('\t')
                for each in toks:
                    if len(each) > 0:
                        entities[each.lower()].add('tag')
        print '<LOAD ENTITIES>%d'%len(entities)
        return entities
    
    def build_pattern_slot(self, dataset):
        entities = self.load_entities()
        patterns = {}
        with open('e:/share/mine_patterns_sorted', 'r') as ifile:
            for line in ifile:
                p_, score = line.strip().split('\t')
                p = Pattern(p_, score)
                patterns[p.pattern] = p
        with open(dataset, 'r') as ifile:
            for idx, line in enumerate(ifile):
                if idx % 1000 == 0:
                    print 'processed %d querys'%(idx)
                if idx >= 100000:
                    break
                q = line.strip().split('\t')[0].lower()
                for pattern_, slot_features in self.iter_patterns(q):
                    if pattern_ in patterns:
                        p_ = patterns[pattern_]
                        p_.match_and_add_to_distribution(q, slot_features, entities)
        pattern_score = defaultdict(float)
        pattern_examples = defaultdict(list)
        for p_ in patterns.itervalues():
            for each_pattern, examples in p_.get_patterns_with_slot_info():
                pattern_score[each_pattern] += p_.score
                pattern_examples[each_pattern].extend(examples)
        with open('e:/share/mine_patterns_sorted_with_slot', 'w') as ofile:
            for pattern_, score in sorted(pattern_score.items(), key=lambda i:i[1], reverse=True):
                print >> ofile, '\t'.join([pattern_, '%.2f'%score, '|'.join(pattern_examples[pattern_])])

if __name__ == '__main__':
    patterns = Patterns()
    patterns.mine_patterns('e:/share/all_node_qa.txt')
    #patterns.mine_patterns('F:/tingting_freq_query/tingting_log_sort.DM.uniquid')
    #patterns.mine_patterns('F:/tingting_freq_query/freq_music_querys')
    #patterns.build_pattern_slot('F:/tingting_freq_query/freq_music_querys')
