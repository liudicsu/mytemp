#coding=utf-8
import sys
import numpy as np
import json
m_dim = 128

def json_query(line):
    j_data = json.loads(line) 
    return j_data["question"].strip().encode('utf-8'), j_data["answer"].strip().encode('utf-8')

def load_file(filename, querys, nodes, need_node=True, need_query=True):
    embedings = []
    with open(filename) as reader:
        for line in reader:
            sps = line.strip().split("\t") 
            if len(sps) != m_dim +1:
                continue
            query, node = json_query(sps[0].strip())
            if need_query:
                querys.append(query)
            if need_node:
                nodes.append(node)
            embedings.append([float(i) for i in sps[1:]]) 
    embedings = np.array(embedings).astype('float32') 
    norm_np = np.linalg.norm(embedings, axis=1) 
    shape_p = norm_np.shape 
    embedings = embedings/norm_np.reshape(shape_p[0],1)
    np.nan_to_num(embedings, False)
    return embedings




def save_pickle_faiss(db_querys, test_querys, test_nodes, mat_result, index_sort, save_file):
    result_dict = {}
    for t_q, m_r, i_s, t_n in zip(test_querys, mat_result, index_sort, test_nodes):
        tmp_tuple_list = []
        #query score same tuple
        tmp_tuple_list.extend((i[1], float(i[0])) for i in np.array([m_r, db_querys[i_s]]).transpose()[0:50] if float(i[0]) > 0.8 and t_q != i[1])
        result_dict[t_q] = tmp_tuple_list

        #tmp = []
        #tmp.append("ANYQ\t"+t_q+"\t"+t_n)  
        #tmp.extend(i[0]+":"+i[1] for i in np.array([m_r, db_querys[i_s]] ).transpose()[0:50] if float(i[0]) > 0.8 and t_q != i[1])
        #print "\t".join(tmp) 
    with open(save_file, "wb") as writer:
        import pickle
        pickle.dump(result_dict, writer)

         
