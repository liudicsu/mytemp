#coding=utf-8
import sys
import filter_file_load
import os

s_set = set()    

def filter(file_name):
    for q, node, q_tmp in filter_file_load.load_file(file_name):
        res_list = [q, node]
        if q in s_set:
            continue

        if len(q.decode("utf-8")) <= 3:
            print >>sys.stderr, "query len is too short：%s\t%s" % (q, node) 
            continue

        for candi_q in q_tmp:
            if len(candi_q.decode("utf-8"))  <= 3:
                continue
            if candi_q in s_set:
                continue
            s_set.add(candi_q)
            res_list.append(candi_q)

        if len(res_list) > 2:
            print "\t".join(res_list)
        else:
            print >>sys.stderr, "res list 0：%s\t%s" % (q, node) 

if __name__ == "__main__":
    file_name = sys.argv[1] 
    filter(file_name)
