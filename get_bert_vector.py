#coding=utf-8
from bert_serving.client import BertClient
import bert_serving.client
import numpy as np
import sys

#print(bert_serving.client.__file__)
def get_input(line_num=1):
	lines = []
	nodes = []	
	l_n = 1
	for line in sys.stdin:
		sps = line.strip().split("\t")	
		node = sps[0]
		data = sps[1]
		lines.append(data)
		nodes.append(node)
		if l_n % line_num == 0:
			yield lines, nodes
			lines.clear()
			nodes.clear()
		l_n += 1
	if len(lines) != 0:
		yield lines, nodes
        

def write_file(nodes, lines, array, wr):
	for node, line, vector in zip(nodes, lines, array):
		tmp = [node, line]
		tmp.extend(str(i) for i in vector)
		wr.write("\t".join(tmp))	
		wr.write("\n")
		

if __name__ == "__main__":
	bc = BertClient()
	line_num = 0

	with open(sys.argv[1], "w") as wr:
		for lines, nodes in get_input(128):
			line_num += 128
			if line_num % 1024 == 0:
				print("line is {}".format(line_num))
			try:
				array = bc.encode(lines)
				norm_np = np.linalg.norm(array, axis=1)
				shape_p = norm_np.shape
				array = array/norm_np.reshape(shape_p[0],1)
				write_file(nodes, lines, array, wr)
			except Exception as e:
				print(e)
