#coding=utf-8
import re
import sys
import filter_file_load

s_set = set()

english = re.compile(ur'[0-9a-zA-Z]+', re.U)
chinese = re.compile(ur'[\u4e00-\u9fa5]', re.U)

def cut(q):
    if type(q) is str: 
        q = q.decode("utf-8")
    return set(english.findall(q)), set(chinese.findall(q))

def jcard(q1_e, q1_c, q2_e, q2_c):
    all_s = q1_e | q1_c | q2_e | q2_c
     
    diff_s = (q1_e & q1_c) ^ (q2_e & q2_c)
    return len(diff_s)*1.0/len(all_s)
    
def filter(file_name):
    for q, node, q_tmp in filter_file_load.load_file(file_name):
        if q in s_set:
            continue
        s_set.add(q) 
        q1_e, q1_c = cut(q)
        res_list = [q, node]

        for candi_q in q_tmp:
            if candi_q in s_set:
                continue
            q2_e, q2_c = cut(candi_q)
            not_sim = jcard(q1_e, q1_c, q2_e, q2_c)

            if not_sim > 0.2: 
                continue    
            s_set.add(candi_q)
            res_list.append(candi_q)

        if len(res_list) > 2:
            print "\t".join(res_list[:13])
        else:
            print >>sys.stderr, "res list 0：%s\t%s" % (q, node) 

if __name__ == "__main__":
    file_name = sys.argv[1] 
    filter(file_name)
