
#cat > ~/AnyQ/build/example/conf/retrieval.conf <<EOF
#retrieval_plugin {
#    name : "semantic_recall"
#    type : "SemanticRetrievalPlugin"
#    vector_size : 128
#    search_k : 10000 
#    index_path : "./example/conf/semantic.annoy"
#    using_dict_name: "annoy_knowledge_dict"
#    num_result : 1
#}
#EOF
#echo return num 1
#echo ====================
##sh single_test.sh 1.0 0.0
#sh single_test.sh 0.0 1.0


cat > ~/AnyQ/build/example/conf/retrieval.conf <<EOF
retrieval_plugin {
    name : "semantic_recall"
    type : "SemanticRetrievalPlugin"
    vector_size : 128
    search_k : 10000 
    index_path : "./example/conf/semantic.annoy"
    using_dict_name: "annoy_knowledge_dict"
    num_result : 2
}
EOF
echo return num 2
echo ====================
#sh single_test.sh 1.0 0.0
sh single_test.sh 0.2 0.8
##sh single_test.sh 0.1 0.9
#sh single_test.sh 0.2 0.8
##sh single_test.sh 0.3 0.7
#sh single_test.sh 0.4 0.6
##sh single_test.sh 0.5 0.5
#sh single_test.sh 0.6 0.4
##sh single_test.sh 0.7 0.3
#sh single_test.sh 0.8 0.2
##sh single_test.sh 0.9 0.1


#cat > ~/AnyQ/build/example/conf/retrieval.conf <<EOF
#retrieval_plugin {
#    name : "semantic_recall"
#    type : "SemanticRetrievalPlugin"
#    vector_size : 128
#    search_k : 10000 
#    index_path : "./example/conf/semantic.annoy"
#    using_dict_name: "annoy_knowledge_dict"
#    num_result : 50
#}
#EOF
#
#
#echo return num 50
#echo ====================
#sh single_test.sh 1.0 0.0
#sh single_test.sh 0.0 1.0
##sh single_test.sh 0.1 0.9
#sh single_test.sh 0.2 0.8
##sh single_test.sh 0.3 0.7
#sh single_test.sh 0.4 0.6
##sh single_test.sh 0.5 0.5
#sh single_test.sh 0.6 0.4
##sh single_test.sh 0.7 0.3
#sh single_test.sh 0.8 0.2
##sh single_test.sh 0.9 0.1
#
#
#cat > ~/AnyQ/build/example/conf/retrieval.conf <<EOF
#retrieval_plugin {
#    name : "semantic_recall"
#    type : "SemanticRetrievalPlugin"
#    vector_size : 128
#    search_k : 10000 
#    index_path : "./example/conf/semantic.annoy"
#    using_dict_name: "annoy_knowledge_dict"
#    num_result : 100
#}
#EOF
#
#
#echo return num 100
#echo ====================
#sh single_test.sh 1.0 0.0
#sh single_test.sh 0.0 1.0
##sh single_test.sh 0.1 0.9
#sh single_test.sh 0.2 0.8
##sh single_test.sh 0.3 0.7
#sh single_test.sh 0.4 0.6
##sh single_test.sh 0.5 0.5
#sh single_test.sh 0.6 0.4
##sh single_test.sh 0.7 0.3
#sh single_test.sh 0.8 0.2
##sh single_test.sh 0.9 0.1
