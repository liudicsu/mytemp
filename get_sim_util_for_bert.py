#coding=utf-8
import sys
import numpy as np
import json
m_dim = 768*4


def load_file(filename, querys, nodes, need_node=True, need_query=True):
    embedings = []
    with open(filename) as reader:
        for line in reader:
            sps = line.strip().split("\t") 
            if len(sps) != m_dim + 2:
                continue
            node, query = sps[:2]
            if need_query:
                querys.append(query)
            if need_node:
                nodes.append(node)
            embedings.append([float(i) for i in sps[2:]]) 
    embedings = np.array(embedings).astype('float32') 
    np.nan_to_num(embedings, False)
    return embedings




def save_pickle_faiss(db_querys, test_querys, db_nodes, test_nodes, mat_result, index_sort, save_file):
    result_dict = {}
    for t_q, m_r, i_s, t_n in zip(test_querys, mat_result, index_sort, test_nodes):
        tmp_tuple_list = []
        #query score same tuple
        tmp_tuple_list.extend((i[1], float(i[0]), i[2]==t_n) for i in np.array([m_r, db_querys[i_s], db_nodes[i_s]]).transpose()[0:10] if float(i[0]) > 0.8 and t_q != i[1])
        result_dict[t_q] = tmp_tuple_list

        tmp = []
        tmp.append("ANYQ\t"+t_q+"\t"+t_n)  
        tmp.extend(i[0]+":"+i[1]+":"+str(i[2]==t_n) for i in np.array([m_r, db_querys[i_s], db_nodes[i_s]]).transpose()[0:10] if float(i[0]) > 0.8 and t_q != i[1])
        print "\t".join(tmp) 
    with open(save_file, "wb") as writer:
        import pickle
        pickle.dump(result_dict, writer)

         
