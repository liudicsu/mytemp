first_step_file=$2
if [[ "${first_step_file}x" == "x" ]];then
	cat $1 |awk -F "\t" 'BEGIN{print "question\tanswer"}{print }' > .t
fi

if [[ "${first_step_file}x" != "x" ]];then
	cat $1|sh $2 |awk -F "\t" 'BEGIN{print "question\tanswer"}{print }' > .t
fi

file_name=`readlink -f .t`
cd ~/AnyQ/build/
python solr_script/make_json.py $file_name faq/schema_format .t >& err
./a_too example/conf/ example/conf/analysis.conf  .t  128  result >& err
result=`readlink -f result`
cd -
mv $result ${1}".vector"
